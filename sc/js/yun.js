// 页面效果，包括一些特效方法，公共方法等。
$(function () {
  (function () {
    //
    $.each($('.header-nav').find('a'), function () {
      var href = $(this).attr('href');
      if (location.href.indexOf(href.substring(2)) > 0) {
        $(this).parent().addClass('on');
      }
    });
    $.each($('.nav-nav').find('a'), function () {
      var href = $(this).attr('href');
      if (location.href.indexOf(href.substring(2)) > 0) {
        $(this).parent().addClass('on');
        // $('.position').find('a').eq(2).text($(this).text());
      }
    });

    var txt=$('.head-nav').find('.nav-nav .on').text();
    if(txt){
      $('.position').find('.this-pos').html('<em>&gt;</em>'+txt);
      $('.aside-nav-tip').find('strong').text(txt);
    }


    $('.type').find('li').on('click', function () {
      if (!$(this).hasClass('on')) {
        $(this).parent().find('.on').removeClass('on');
        $(this).addClass('on');
      }
    });
    $.each($('a'),function () {
      if(!$(this).attr('href')){
        console.log($(this).attr('href'));
        $(this).attr('href','javascript:');
      }
    });

  })();
  // 功能组件
  (function () {
    // 选项卡
    $.fn.yunTab = function (option) {
      var defaults = {
        tabBar: '.tab-btn',
        tabCon: '.tab-content',
        className: 'this-btn',
        index: 0,
        event:'click'
      };
      var options = $.extend(defaults, option);
      var that = $(this);
      that.find(options.tabBar).removeClass(options.className)
        .eq(options.index).addClass(options.className);
      that.find(options.tabCon).hide()
        .eq(options.index).show();
      that.on(options.event, options.tabBar, function () {
        if (!$(this).hasClass(options.className)) {
          that.find(options.tabBar).removeClass(options.className);
          $(this).addClass(options.className);
          var index = that.find(options.tabBar).index(this);
          that.find(options.tabCon).hide().eq(index).stop().fadeIn(200);
        }
      });
    };
    //左右滑动轮播
    $.fn.slideBanner = function (options) {
      var setting = $.extend({
          speed: 3000,
          arrow: true,
          hover: true
        }, options),
        that = this,
        bannerUi = that.find('.banner-ul'),
        bannerLi = bannerUi.find('li'),
        arrow = that.find('.banner-arrow'),
        nextBtn = that.find('.next'),
        prevBtn = that.find('.prev'),
        bannerBtn = that.find('.banner-btn').find('li'),
        timer = null,
        state = true,
        index = 0,
        sMove = bannerUi.outerWidth(),
        liNum = bannerLi.length;
      bannerLi.first().css('left', 0);

      function focus() {
        bannerBtn.removeClass('on');
        bannerBtn.eq(index).addClass('on');
      }

      if (!setting.arrow) {
        arrow.css('display', 'none');
      }
      bannerBtn.on('click', function () {
        var thisIndex = $(this).index();
        if (thisIndex > index) {
          bannerLi.eq(index).animate({left: -sMove});
          bannerLi.eq(thisIndex).css({left: sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        } else if (thisIndex < index) {
          bannerLi.eq(index).animate({left: sMove});
          bannerLi.eq(thisIndex).css({left: -sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        }
      });

      function play() {
        bannerLi.eq(index).animate({left: -sMove});
        index++;
        if (index === liNum) {
          index = 0;
          bannerLi.eq(index).css({left: sMove});
        }
        bannerLi.eq(index).css({left: sMove}).animate({left: 0});
        focus();
      }

      function autoPlay() {
        timer = setInterval(play, setting.speed);
      }

      if (setting.hover) {
        that.hover(function () {
          clearInterval(timer);
          timer = null;
        }, function () {
          autoPlay();
        });
      }
      prevBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: sMove});
          index--;
          if (index < 0) {
            index = liNum - 1;
          }
          bannerLi.eq(index).css({left: -sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      nextBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: -sMove});
          index++;
          if (index === liNum) {
            index = 0;
          }
          bannerLi.eq(index).css({left: sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      autoPlay();
      if (liNum === 1) {
        clearInterval(timer);
        timer = null;
      }
      return this;
    };
    //字数限制
    $.fn.limitWords = function (max) {
      this.each(function () {
        var text = $.text($(this));
        if (text.length > max) {
          this.innerHTML = text.substring(0, max) + '...';
        }
      });
      return this;
    };
    //slide淡入淡出轮播
    $.fn.fadingSlide = function (options) {
      var settings = $.extend({
          'speed': '3000'
        }, options),
        that = $(this),
        btn = that.find('ul.slide-btn').find('li'),
        imgLi = that.find('ul.slide-img').find('li'),
        ind = 0,
        length = btn.length,
        timer = null;

      function play(ind) {
        btn.removeClass('on').eq(ind).addClass('on');
        imgLi.removeClass('up').eq(ind).addClass('up');
      }

      function witchplay(witch) {
        if (witch > 0) {
          if (ind === length - 1) {
            ind = 0;
            play(ind);
          } else {
            ind++;
            play(ind);
          }
        } else {
          if (ind === 0) {
            ind = length - 1;
            play(ind);
          } else {
            ind--;
            play(ind);
          }
        }
      }

      function autoplay(speed) {
        timer = setInterval(function () {
          witchplay(1);
        }, speed);
      }

      autoplay(settings.speed);
      btn.click(function () {
        ind = $(this).index();
        play(ind);
      });
      that.mousemove(function () {
        clearInterval(timer);
        timer = null;
      }).mouseout(function () {
        autoplay(settings.speed);
      });
      return this;
    };
    //无缝滚动
    $.fn.rolling = function (options) {
      var settings = $.extend({
        left: true,
        toLeft: true,
        speed: 1
      }, options);
      var that = this, oLi = that.find('li'), d = 0;

      function clearTime(fn) {
        var timer = setInterval(fn, 25);
        that.hover(function () {
          clearInterval(timer);
          timer = null;
        }, function () {
          timer = setInterval(fn, 25);
        });
      }

      if (settings.left) {
        var wi = oLi.length * oLi.outerWidth(true) * 2;
        that.css({width: wi}).html(that.html() + that.html())
          .parent().css({width: wi / 2, overflow: 'hidden'});
        var move = function () {
          if (settings.toLeft) {
            if (-d >= wi / 2) {
              d = 0;
              that.css('left', 0);
            } else {
              d = d - settings.speed;
              that.css('left', d);
            }
          } else {
            that.css('left', -wi / 2);
            if (d >= wi / 2) {
              d = 0;
              that.css('left', -wi / 2);
            } else {
              d = d + settings.speed;
              that.css('left', d - (wi / 2));
            }
          }
        };
        clearTime(move);
      } else {
        var He = oLi.length * oLi.outerHeight(true) * 2;
        that.css({height: He}).html(that.html() + that.html())
          .parent().css({height: He / 2, overflow: 'hidden'});
        var moveTop = function () {
          if (-d >= He / 2) {
            d = 0;
            that.css('top', 0);
          } else {
            d = d - settings.speed;
            that.css('top', d);
          }
        };
        clearTime(moveTop);
      }
      return this;
    };
    // 单项选择
    $.fn.chooseOne=function (options) {
      var settings = $.extend({
          'className': 'on'
        }, options),
        that = $(this);
      that.on('click',function () {
        if(!$(this).hasClass(settings.className)){
          $(this).parent().find('.'+settings.className).removeClass(settings.className);
          $(this).addClass(settings.className);
        }
      });
    }


  })();


});

