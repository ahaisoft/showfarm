require.config({
  baseUrl: '../assets/',
  paths: {
    // jquery: ['jquery-3.2.1.min','https://code.jquery.com/jquery-3.2.1.min.js'],
    mock: ['mock', 'http://mockjs.com/dist/mock']
  },
  shim: {
    /*myJsCode: {
      deps: ['jquery', 'mock'],
      exports: ''
    }*/
  }
});
//,'jquery'
require(['mock'], function (Mock) {
  //货品包装
  (function () {
    Mock.mock('packup-table-head', {
      'list': [{
        cid: '编号',
        cname: '包装名称',
        state: '状态',
        particulars: '详情'
      }]
    });
    Mock.mock('packup-table-body', {
      'list|15': [{
        cid: '@integer(1000,10000)',
        cname: '@cparagraph(1)',
        state: '@boolean'
      }]
    });
  })();

  //货品渠道
  (function () {
    Mock.mock('channel-table-head', {
      'list': [{
        cid: '编号',
        cmark: '渠道标示',
        cname: '渠道名称',
        state: '状态',
        particulars: '详情'
      }]
    });
    Mock.mock('channel-table-body', {
      'list|10': [{
        cid: '@integer(1000,10000)',
        cmark: '@word()',
        cname: '@cparagraph(1)',
        state: '@boolean'
      }]
    });
  })();

  //品标维护
  (function () {
    Mock.mock('goodsunit-table-head', {
      'list': [{
        cid: '编号',
        cmark: '标示',
        cicon: '图标',
        cname: '名称',
        type: '类型',
        particulars: '详情'
      }]
    });
    Mock.mock('goodsunit-table-body', {
      'list|10': [{
        cid: '@integer(1000,10000)',
        cmark: '@word()',
        cicon: '@image(40x40)',
        cname: '@cparagraph(1)',
        state: '@boolean'
      }]
    });
  })();

  //快递编码
  (function () {
    Mock.mock('delivery-table-head', {
      'list': [{
        cid: '编号',
        cmark: '快递公司编号',
        cicon: '快递公司',
        cname: '公司类型',
        type: '物流公司ID'
      }]
    });
    Mock.mock('delivery-table-body', {
      'list|10': [{
        cid: '@integer(1000,10000)',
        cmark: '@cparagraph(1)',
        cicon: '@image(40x40)',
        cname: '@cparagraph(1)',
        type: '@boolean'
      }]
    });
  })();

  (function () {
    Mock.mock('sylist-table-head', {
      'list': [{
        cid: '公司编号',
        cname: '公司名称',
        gid: '货品编号',
        pimg: '产品图片',
        gname: '货品名称',
        bid: '货品批次号',
        bname: '批次名称',
        baseid: '基地编号',
        addtime: '创建时间'
      }]
    });
    Mock.mock('sylist-table-body', {
      'list|15': [{
        cid: '@integer(1000,10000)',
        cname: '@cparagraph(1)',
        gid: '@integer(1000,10000)',
        pimg: '@image(400x400,@color)',
        gname: '@cparagraph(1)',
        bid: '@integer(1000,10000)',
        bname: '@cparagraph(1)',
        baseid: '@cparagraph(1)',
        addtime: '@date'
      }]
    });
  })();

  (function () {
    Mock.mock('table-head', {
      'list': [{
        cid: '公司编号',
        cname: '公司名称',
        gid: '货品编号',
        gname: '货品名称',
        bid: '批次编号',
        bname: '批次名称',
        pname: '工序名称',
        otime: '操作时间',
        pnum: '加工人数',
        psum: '加工数量',
        state: '审核状态',
        addtime: '添加时间',
        opt: '操作'
      }]
    });
    Mock.mock('table-body', {
      'list|15': [{
        cid: '@integer(1000,10000)',
        cname: '@cparagraph(1)',
        gid: '@integer(1000,10000)',
        gname: '@cparagraph(1)',
        bid: '@integer(1000,10000)',
        bname: '@cparagraph(1)',
        pname: '@cparagraph(1)',
        otime: '@date',
        pnum: '@integer(60,100)',
        psum: '@integer(1000,10000)',
        state: '@boolean',
        addtime: '@date',
        opt: ''
      }]
    });
  })();

  //操作日志
  (function () {
    Mock.mock('log-table-head', {
      'list': [{
        cid: '操作用户ID',
        cname: '操作用户名称',
        ctime: '操作时间',
        cip: '操作外置IP',
        des: '描述',
        remark: '备注'
      }]
    });
    Mock.mock('log-table-body', {
      'list|10': [{
        cid: '@integer(1000,10000)',
        cname: '@cparagraph(1)',
        ctime: '@date',
        cip: '@cparagraph(1)'
      }]
    });
  })();

  Mock.mock('area-tree', {
    tree: [
      {
        name: '中国',
        spread: true,  //默认展开
        children: [
          {
            name: '浙江省',
            children:[
              {
                name:'宁波市',
                upname:'浙江省',
                children:[
                  {name:'鄞州区'},
                  {name:'江北区'},
                  {name:'镇海区'}
                ]
              }
            ]
          },
          {
            name: '上海市',
            upname:'中国',
            children:[
              {name:'徐家汇'},
              {name:'陆家嘴'},
              {name:'外滩'}
            ]
          }
        ]
      },
      {
        name: '美国',
        id:2,
        spread: true,  //默认展开
        children: [
          {name: '加州'}
        ]
      }
    ]
  });

});