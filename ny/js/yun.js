/**
 *
 *----------Dragon be here!----------/
 * 　　 ┏┓　     ┏┓
 * 　　┏┛┻━━━━━━━┛┻┓
 * 　　┃           ┃
 * 　　┃     ━     ┃
 * 　　┃  ┳┛   ┗┳  ┃
 * 　　┃           ┃
 * 　　┃     ┻     ┃
 * 　　┃           ┃
 * 　　┗━┓       ┏━┛
 * 　　　　┃　　　┃神兽保佑
 * 　　　　┃　　　┃代码无BUG！
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃          ┣┓
 * 　　　　┃          ┏┛
 * 　　　　┗━┓┓┏━━━┳┓┏┛
 * 　　　　┃┫┫　 ┃┫┫
 * 　　　　┗┻┛　 ┗┻┛
 * ━━━━━━神兽出没━━━━━━by:coder-pig
 */
// 弹出层
/*参数解释：
	title 打开页面的标题
	url		请求页面的url
	arr	  一个数组，第一个是弹出层宽度w（默认：1000px），第二个是弹出层高度h（默认：窗口高度-50px）
	obj   layer的其他参数，常用的有success回调函数，yes回调函数，btn配置对象等
*/
/*iframe页面获取父页面元素的方法：
$('.tbody',parent.document) //第一个参数是父页面的元素选择器，第二个参数是固定的，意思是在parent.document中获取
*/

/*父页面获取iframe页面元素的方法：
在弹出层的回调函数中，如success回调函数：
success:function (obj,index) {
          var body=layer.getChildFrame('body',index);  //获取iframe页面的body元素，赋值给变量body
          console.log($('.de-title',body).html());     //获取方法同上
        }
*/
/*
function dialogShow(title, url, arr, obj) {
  if (!title) {
    title = false;
  }
  if (!url) {
    url = "404.html";
  }
  if (!arr[0]) {
    arr[0] = 1000;
  }
  if (!arr[1]) {
    arr[1] = ($(window).height() - 50);
  }
  if (typeof obj !== 'object') {
    obj = {};
  }
  var opt = $.extend({
    type: 2,
    area: [arr[0] + 'px', arr[1] + 'px'],
    title: title,
    content: url+'.html',
    resize: false,
    scrollbar:false,
    btn:['关闭']
    /!*shadeClose:true,
    moveOut: true*!/
    /!*anim: 2,
    isOutAnim: false*!/
  }, obj);
  layui.layer.open(opt);
}

// 子页面关闭自身弹出框，如需要父页面关闭弹出框请用：layer.close(index);
function dialogClose() {
  var index = parent.layer.getFrameIndex(window.name);
  parent.layer.close(index);
}

// 确认框，一般用在确认删除，登录等场景
/!*参数解释：
	html  询问的内容，可以是html标签（建议以变量形式传入），也可以纯文本。
	obj   layer的其他参数，常用的有success回调函数，yes回调函数，btn配置对象等
*!/
function confirm(html, obj) {
  if (!html) {
    html = "请输入需要确认的问题(⊙o⊙)";
  }
  if (typeof obj !== 'object') {
    obj = {};
  }
  var opt = $.extend({
    closeBtn: 2,
    content: html,
    resize: false,
    title: false
  }, obj);
  layui.layer.open(opt);
}

function photos(target, obj) {
  var opt = $.extend({
    photos: target,
    anim: -1,
    shade: 0.3
  }, obj);
  layui.layer.photos(opt);
}

// 黑背景透明的提醒
function msg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {time: time});
}

// 操作有误提醒消息
function errorMsg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {time: time, anim: '6'});
}

// 操作成功提醒消息
function successMsg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {icon: 6, time: time, anim: '5'});
}
*/

// 页面效果，包括一些特效方法，公共方法等。
// 防抖
function debounce(fn, delay) {
  // 维护一个 timer
  var timer = null;
  return function() {
    // 通过 ‘this’ 和 ‘arguments’ 获取函数的作用域和变量
    var context = this;
    var args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function() {
      fn.apply(context, args);
    }, delay);
  }
}
//节流
function throttle(func,delay){
  var prev = new Date().getTime();
  return function(){
    var context = this;
    var args = arguments;
    var now = new Date().getTime();
    if(now-prev>=delay){
      func.apply(context,args);
      prev = new Date().getTime();
    }
  }
}
$(function () {
  (function () {
    /*$.each($('.header-nav').find('a'), function () {
      var href = $(this).attr('href');
      if (location.href.indexOf(href.substring(2)) > 0) {
        $(this).parent().addClass('on');
      }
    });*/
    /*$.each($('.nav-one').find('a'), function () {
      var href = $(this).attr('href');
      if (location.href.indexOf(href.substring(2)) > 0) {
        $(this).parents('.nav-one-li').addClass('on');
        $('.position').find('a').eq(2).text($(this).text());
      }
    });*/
    $('.nav-one').find('.nav-arrow').on('click',function () {
      if($(this).parent().find('.nav-two').length){
        $('.nav-two').stop().slideToggle(200);
        $(this).toggleClass('rotate');
      }
    });
    if (location.href.indexOf('/y-index/index.html') < 0) {
      $('.header-search').hide();
      $('.top-search-btn').hide();
    }
  })();
  // 功能组件
  (function () {
    // 选项卡
    $.fn.yunTab = function (option) {
      var defaults = {
        tabBar: '.tab-btn',
        tabCon: '.tab-content',
        className: 'this-btn',
        index: 0
      };
      var options = $.extend(defaults, option);
      var that = $(this);
      that.find(options.tabBar).removeClass(options.className)
        .eq(options.index).addClass(options.className);
      that.find(options.tabCon).hide()
        .eq(options.index).show();
      that.on('click', options.tabBar, function () {
        if (!$(this).hasClass(options.className)) {
          that.find(options.tabBar).removeClass(options.className);
          $(this).addClass(options.className);
          var index = that.find(options.tabBar).index(this);
          that.find(options.tabCon).hide().eq(index).fadeIn(200);
        }
      });
    };
    //左右滑动轮播
    $.fn.slideBanner = function (options) {
      var setting = $.extend({
          speed: 3000,
          arrow: true,
          hover: true
        }, options),
        that = this,
        bannerUi = that.find('.banner-ul'),
        bannerLi = bannerUi.find('li'),
        arrow = that.find('.banner-arrow'),
        nextBtn = that.find('.next'),
        prevBtn = that.find('.prev'),
        bannerBtn = that.find('.banner-btn').find('li'),
        timer = null,
        state = true,
        index = 0,
        sMove = bannerUi.outerWidth(),
        liNum = bannerLi.length;
      bannerLi.first().css('left', 0);

      function focus() {
        bannerBtn.removeClass('on');
        bannerBtn.eq(index).addClass('on');
      }

      if (!setting.arrow) {
        arrow.css('display', 'none');
      }
      bannerBtn.on('click', function () {
        var thisIndex = $(this).index();
        if (thisIndex > index) {
          bannerLi.eq(index).animate({left: -sMove});
          bannerLi.eq(thisIndex).css({left: sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        } else if (thisIndex < index) {
          bannerLi.eq(index).animate({left: sMove});
          bannerLi.eq(thisIndex).css({left: -sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        }
      });

      function play() {
        bannerLi.eq(index).animate({left: -sMove});
        index++;
        if (index === liNum) {
          index = 0;
          bannerLi.eq(index).css({left: sMove});
        }
        bannerLi.eq(index).css({left: sMove}).animate({left: 0});
        focus();
      }

      function autoPlay() {
        timer = setInterval(play, setting.speed);
      }

      if (setting.hover) {
        that.hover(function () {
          clearInterval(timer);
          timer = null;
        }, function () {
          autoPlay();
        });
      }
      prevBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: sMove});
          index--;
          if (index < 0) {
            index = liNum - 1;
          }
          bannerLi.eq(index).css({left: -sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      nextBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: -sMove});
          index++;
          if (index === liNum) {
            index = 0;
          }
          bannerLi.eq(index).css({left: sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      autoPlay();
      if (liNum === 1) {
        clearInterval(timer);
        timer = null;
      }
      return this;
    };
    //字数限制
    $.fn.limitWords = function (max) {
      this.each(function () {
        var text = $.text($(this));
        if (text.length > max) {
          this.innerHTML = text.substring(0, max) + '...';
        }
      });
      return this;
    };
    //slide淡入淡出轮播
    $.fn.fadingSlide = function (options) {
      var settings = $.extend({
          'speed': '3000'
        }, options),
        that = $(this),
        btn = that.find('ul.slide-btn').find('li'),
        imgLi = that.find('ul.slide-img').find('li'),
        ind = 0,
        length = btn.length,
        timer = null;

      function play(ind) {
        btn.removeClass('on').eq(ind).addClass('on');
        imgLi.removeClass('up').eq(ind).addClass('up');
      }

      function witchplay(witch) {
        if (witch > 0) {
          if (ind === length - 1) {
            ind = 0;
            play(ind);
          } else {
            ind++;
            play(ind);
          }
        } else {
          if (ind === 0) {
            ind = length - 1;
            play(ind);
          } else {
            ind--;
            play(ind);
          }
        }
      }

      function autoplay(speed) {
        timer = setInterval(function () {
          witchplay(1);
        }, speed);
      }

      autoplay(settings.speed);
      btn.click(function () {
        ind = $(this).index();
        play(ind);
      });
      that.mouseover(function () {
        clearInterval(timer);
        timer = null;
      }).mouseout(function () {
        autoplay(settings.speed);
      });
      return this;
    };
    //无缝滚动
    $.fn.rolling = function (options) {
      var settings = $.extend({
        left: true,
        toLeft: true,
        speed: 1
      }, options);
      var that = this, oLi = that.find('li'), d = 0;

      function clearTime(fn) {
        var timer = setInterval(fn, 25);
        that.hover(function () {
          clearInterval(timer);
          timer = null;
        }, function () {
          timer = setInterval(fn, 25);
        });
      }

      if (settings.left) {
        var wi = oLi.length * oLi.outerWidth(true) * 2;
        that.css({width: wi}).html(that.html() + that.html())
          .parent().css({width: wi / 2, overflow: 'hidden'});
        var move = function () {
          if (settings.toLeft) {
            if (-d >= wi / 2) {
              d = 0;
              that.css('left', 0);
            } else {
              d = d - settings.speed;
              that.css('left', d);
            }
          } else {
            that.css('left', -wi / 2);
            if (d >= wi / 2) {
              d = 0;
              that.css('left', -wi / 2);
            } else {
              d = d + settings.speed;
              that.css('left', d - (wi / 2));
            }
          }
        };
        clearTime(move);
      } else {
        var He = oLi.length * oLi.outerHeight(true) * 2;
        that.css({height: He}).html(that.html() + that.html())
          .parent().css({height: He / 2, overflow: 'hidden'});
        var moveTop = function () {
          if (-d >= He / 2) {
            d = 0;
            that.css('top', 0);
          } else {
            d = d - settings.speed;
            that.css('top', d);
          }
        };
        clearTime(moveTop);
      }
      return this;
    };
  })();
});

