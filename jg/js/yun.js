/**
 *
 *----------Dragon be here!----------/
 * 　　 ┏┓　     ┏┓
 * 　　┏┛┻━━━━━━━┛┻┓
 * 　　┃           ┃
 * 　　┃     ━     ┃
 * 　　┃  ┳┛   ┗┳  ┃
 * 　　┃           ┃
 * 　　┃     ┻     ┃
 * 　　┃           ┃
 * 　　┗━┓       ┏━┛
 * 　　　　┃　　　┃神兽保佑
 * 　　　　┃　　　┃代码无BUG！
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃          ┣┓
 * 　　　　┃          ┏┛
 * 　　　　┗━┓┓┏━━━┳┓┏┛
 * 　　　　┃┫┫　 ┃┫┫
 * 　　　　┗┻┛　 ┗┻┛
 * ━━━━━━神兽出没━━━━━━by:coder-pig
 */
// 弹出层
/*参数解释：
	title 打开页面的标题
	url		请求页面的url
	arr	  一个数组，第一个是弹出层宽度w（默认：1000px），第二个是弹出层高度h（默认：窗口高度-50px）
	obj   layer的其他参数，常用的有success回调函数，yes回调函数，btn配置对象等
*/
/*iframe页面获取父页面元素的方法：
$('.tbody',parent.document) //第一个参数是父页面的元素选择器，第二个参数是固定的，意思是在parent.document中获取
*/

/*父页面获取iframe页面元素的方法：
在弹出层的回调函数中，如success回调函数：
success:function (obj,index) {
          var body=layer.getChildFrame('body',index);  //获取iframe页面的body元素，赋值给变量body
          console.log($('.de-title',body).html());     //获取方法同上
        }
*/
function dialogShow(title, url, arr, obj) {
  if (!title) {
    title = false;
  }
  if (!url) {
    url = "404.html";
  }
  if (!arr[0]) {
    arr[0] = 1000;
  }
  if (!arr[1]) {
    arr[1] = ($(window).height() - 50);
  }
  if (typeof obj !== 'object') {
    obj = {};
  }
  var opt = $.extend({
    type: 2,
    area: [arr[0] + 'px', arr[1] + 'px'],
    title: title,
    content: url,
    resize: false
    /*shadeClose:true,
    moveOut: true*/
    /*anim: 2,
    isOutAnim: false*/
  }, obj);
  layui.layer.open(opt);
}

// 子页面关闭自身弹出框，如需要父页面关闭弹出框请用：layer.close(index);
function dialogClose() {
  var index = parent.layer.getFrameIndex(window.name);
  parent.layer.close(index);
}

// 确认框，一般用在确认删除，登录等场景
/*参数解释：
	html  询问的内容，可以是html标签（建议以变量形式传入），也可以纯文本。
	obj   layer的其他参数，常用的有success回调函数，yes回调函数，btn配置对象等
*/
function confirm(html, obj) {
  if (!html) {
    html = "请输入需要确认的问题(⊙o⊙)";
  }
  if (typeof obj !== 'object') {
    obj = {};
  }
  var opt = $.extend({
    closeBtn: 2,
    content: html,
    resize: false,
    title: false
  }, obj);
  layui.layer.open(opt);
}

function photos(target, obj) {
  var opt = $.extend({
    photos: target,
    anim: -1,
    shade: 0.3
  }, obj);
  layui.layer.photos(opt);
}

// 黑背景透明的提醒
function msg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {time: time});
}

// 操作有误提醒消息
function errorMsg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {time: time, anim: '6'});
}

// 操作成功提醒消息
function successMsg(title, time) {
  if (!time) {
    time = 1500
  }
  layui.layer.msg(title, {icon: 6, time: time, anim: '5'});
}

// 页面效果，包括一些特效方法，公共方法等。
$(function () {
  // 表头全选按钮
  (function () {
    var tbody = $('tbody.tbody'), checkall, inputs, input, sum, num;

    function isAll() {
      inputs = tbody.find('input[type=checkbox]');
      sum = inputs.length;
      num = inputs.filter(':checked').length;
      checkall = $('#checkall');
      num === sum ? checkall.prop('checked', true) : checkall.prop('checked', false);
    }

    $('thead.thead').on('click', '#checkall', function (e) {
      e.stopPropagation();
      inputs = tbody.find('input[type=checkbox]');
      $(this).is(':checked') ? inputs.prop('checked', true) : inputs.prop('checked', false);
    });
    // 当表内容选择按钮全选时自动变成全选
    tbody.on('click', 'input[type=checkbox]', function (e) {
      e.stopPropagation();
      isAll();
    }).on('click', 'tr', function (e) {
      e.stopPropagation();
      input = $(this).find('input[type=checkbox]');
      input.is(':checked') ? input.prop('checked', false) : input.prop('checked', true);
      isAll();
    });
  })();


  (function () {
    //
    $.each($('#aside').find('a'), function () {
      var href = $(this).attr('href');
      if (location.href.indexOf(href.substring(2)) > 0) {
        $('li.menu-ul-two-show').removeClass('menu-ul-two-show');
        $(this).parents('.menu-li-two').addClass('on').parents('.menu-li-one').addClass('menu-ul-two-show');
      }
    });
  })();

  // 侧栏显示隐藏效果
  (function () {
    var aside = $('#aside');
    var mainBody = $('#main-body');
    $.each($('.menu-li-two'), function () {
      $(this).attr('title', $(this).find('>a>span').text());
    });


    function showHide(is) {
      if (is) {
        aside.addClass('aside-hide');
        mainBody.addClass('main-body-hide');
        aside.find('i.aside-show-btn-icon').addClass('rotate90');
      } else {
        aside.removeClass('aside-hide');
        mainBody.removeClass('main-body-hide');
        aside.find('i.aside-show-btn-icon').removeClass('rotate90');
      }
    }

    /*$(window).on('resize',function () {
      showHide($(this).width()<1300);
    });
    showHide($(this).width()<1300);*/
    aside.on('click', 'div.aside-show-btn', function (e) {
      e.stopPropagation();
      showHide(!aside.hasClass('aside-hide'))
    }).on('click', '.menu-title', function (e) {
      e.stopPropagation();
      var ulTwo = $(this).parent().find('ul.menu-ul-two');
      if (ulTwo.length) {
        if ($(this).parent().hasClass('menu-ul-two-show')) {
          $(this).parent().toggleClass('menu-ul-two-show');
        } else {
          $('li.menu-ul-two-show').removeClass('menu-ul-two-show');
          $(this).parent().addClass('menu-ul-two-show');
        }
        /*$('ul.menu-ul-two').slideUp();
        ulTwo.stop().slideToggle();*/
      }
    }).on('mouseenter', '.menu-li-two', function (e) {
      e.stopPropagation();
      if ($(this).find('.menu-li-two-menu').length) {
        $(this).addClass('aside-thisarrow');
      }
    }).on('mouseleave', '.menu-li-two', function (e) {
      e.stopPropagation();
      $(this).removeClass('aside-thisarrow');
    });

  })();
  // 功能组件
  (function () {
    // 选项卡
    $.fn.yunTab = function (option) {
      var defaults = {
        tabBar: '.tab-btn',
        tabCon: '.tab-content',
        className: 'this-btn',
        index: 0
      };
      var options = $.extend(defaults, option);
      var that = $(this);
      that.find(options.tabBar).removeClass(options.className)
        .eq(options.index).addClass(options.className);
      that.find(options.tabCon).hide()
        .eq(options.index).show();
      that.on('click', options.tabBar, function () {
        if (!$(this).hasClass(options.className)) {
          that.find(options.tabBar).removeClass(options.className);
          $(this).addClass(options.className);
          var index = that.find(options.tabBar).index(this);
          that.find(options.tabCon).hide().eq(index).fadeIn(200);
        }
      });
    };
    //年月选择
    $.fn.setYear = function (setYear) {//设置近N年
      var that = this;
      var $month = $('#month');
      var date = new Date();
      var year = date.getFullYear();
      var month = date.getMonth();
      var yearList = '', monthList = '';
      for (var i = year; i > year - setYear; i--) {
        yearList += '<option>' + i + '</option>';
      }
      for (var k = month; k > 0; k--) {
        monthList += '<option>' + k + '</option>';
      }
      that.empty().append(yearList);
      $month.empty().append(monthList);
      that.on('change', function () {
        monthList = '';
        if (parseInt($(this).val()) < year) {
          for (var i = 12; i > 0; i--) {
            monthList += '<option>' + i + '</option>';
          }
          $month.empty().append(monthList);
        } else if (parseInt($(this).val()) === year) {
          for (var k = month; k > 0; k--) {
            monthList += '<option>' + k + '</option>';
          }
          $month.empty().append(monthList);
        }
      });
    };
    //左右滑动轮播
    $.fn.slideBanner = function (options) {
      var setting = $.extend({
          speed: 3000,
          arrow: true,
          hover: true
        }, options),
        that = this,
        bannerUi = that.find('.banner-ul'),
        bannerLi = bannerUi.find('li'),
        arrow = that.find('.banner-arrow'),
        nextBtn = that.find('.next'),
        prevBtn = that.find('.prev'),
        bannerBtn = that.find('.banner-btn').find('li'),
        timer = null,
        state = true,
        index = 0,
        sMove = bannerUi.outerWidth(),
        liNum = bannerLi.length;
      bannerLi.first().css('left', 0);

      function focus() {
        bannerBtn.removeClass('on');
        bannerBtn.eq(index).addClass('on');
      }

      if (!setting.arrow) {
        arrow.css('display', 'none');
      }
      bannerBtn.on('click', function () {
        var thisIndex = $(this).index();
        if (thisIndex > index) {
          bannerLi.eq(index).animate({left: -sMove});
          bannerLi.eq(thisIndex).css({left: sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        } else if (thisIndex < index) {
          bannerLi.eq(index).animate({left: sMove});
          bannerLi.eq(thisIndex).css({left: -sMove}).animate({left: 0});
          index = thisIndex;
          focus();
        }
      });

      function play() {
        bannerLi.eq(index).animate({left: -sMove});
        index++;
        if (index === liNum) {
          index = 0;
          bannerLi.eq(index).css({left: sMove});
        }
        bannerLi.eq(index).css({left: sMove}).animate({left: 0});
        focus();
      }

      function autoPlay() {
        timer = setInterval(play, setting.speed);
      }

      if (setting.hover) {
        that.hover(function () {
          clearInterval(timer);
          timer = null;
        }, function () {
          autoPlay();
        });
      }
      prevBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: sMove});
          index--;
          if (index < 0) {
            index = liNum - 1;
          }
          bannerLi.eq(index).css({left: -sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      nextBtn.click(function () {
        if (state === true) {
          state = false;
          bannerLi.eq(index).animate({left: -sMove});
          index++;
          if (index === liNum) {
            index = 0;
          }
          bannerLi.eq(index).css({left: sMove}).animate({left: 0}, function () {
            state = true;
          });
          focus();
        }
      });
      autoPlay();
      if (liNum === 1) {
        clearInterval(timer);
        timer = null;
      }
      return this;
    };
  })();
});
/*// ie8不支持map方法
if (!Array.prototype.map) {
  Array.prototype.map = function(callback, thisArg) {
    var T, A, k;
    if (this === null) {throw new TypeError(" this is null or not defined");}
    var O = Object(this);
    var len = O.length >>> 0;
    if (typeof callback !== "function") {throw new TypeError(callback + " is not a function");}
    if (thisArg) {T = thisArg;}
    A = new Array(len);
    k = 0;
    while(k < len) {
      var kValue, mappedValue;
      if (k in O) {
        kValue = O[ k ];
        mappedValue = callback.call(T, kValue, k, O);
        A[ k ] = mappedValue;
      }
      k++;
    }
    return A;
  };
}*/

// ie8表格隔行变色方法（参数为tr的所有对象）
function trAddColor(trs) {
  if (!window.addEventListener) {
    $.each(trs, function (ind, tr) {
      if (ind % 2 !== 0) {
        $(tr).addClass('tr2');
      }
    });
  }
}
