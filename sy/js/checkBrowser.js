+function () {
  var ua = navigator.userAgent.toLowerCase();
  if (ua.indexOf('msie') > -1) {
    var safariVersion = ua.match(/msie ([\d.]+)/)[1];
    if (parseInt(safariVersion) <= 7) {
      window.location.href = './updateBrowser.html';
    }
  }
}();