require.config({
  baseUrl: '../assets/',
  paths: {
    // jquery: ['jquery-3.2.1.min','https://code.jquery.com/jquery-3.2.1.min.js'],
    mock: ['mock', 'http://mockjs.com/dist/mock']
  },
  shim: {
    /*myJsCode: {
      deps: ['jquery', 'mock'],
      exports: ''
    }*/
  }
});
//,'jquery'
require(['mock'], function (Mock) {
  Mock.setup({
    timeout: '200-400'
  });
  Mock.mock('table-head', {
    'list': [{
      cid: '公司编号',
      cname: '公司名称',
      gid: '货品编号',
      gname: '货品名称',
      bid: '批次编号',
      bname: '批次名称',
      pname: '工序名称',
      otime: '操作时间',
      pnum: '加工人数',
      psum: '加工数量',
      state: '审核状态',
      addtime: '添加时间',
      opt: '操作'
    }]
  });
  Mock.mock('table-body', {
    'list|15': [{
      cid: '@integer(1000,10000)',
      cname: '@cparagraph(1)',
      gid: '@integer(1000,10000)',
      gname: '@cparagraph(1)',
      bid: '@integer(1000,10000)',
      bname: '@cparagraph(1)',
      pname: '@cparagraph(1)',
      otime: '@date',
      pnum: '@integer(60,100)',
      psum: '@integer(1000,10000)',
      state: '@boolean',
      addtime: '@date',
      opt: ''
    }]
  });

  Mock.mock('sylist-table-head', {
    'list': [{
      cid: '公司编号',
      cname: '公司名称',
      gid: '货品编号',
      pimg: '产品图片',
      gname: '货品名称',
      bid: '货品批次号',
      bname: '批次名称',
      baseid: '基地编号',
      addtime: '创建时间'
    }]
  });
  Mock.mock('sylist-table-body', {
    'list|15': [{
      cid: '@integer(1000,10000)',
      cname: '@cparagraph(1)',
      gid: '@integer(1000,10000)',
      pimg: '@image(400x400,@color)',
      gname: '@cparagraph(1)',
      bid: '@integer(1000,10000)',
      bname: '@cparagraph(1)',
      baseid: '@cparagraph(1)',
      addtime: '@date'
    }]
  });

  Mock.mock('tree', {
    tree: [
      {
        "name": "常用文件夹",
        "id": 1,
        "alias": "changyong",
        "children": [
          {
            "name": "所有未读",
            "id": 11,
            "href": "http://www.layui.com/",
            "alias": "weidu"
          },
          {
            "name": "置顶邮件",
            "id": 12
          },
          {
            "name": "标签邮件",
            "id": 13
          }
        ]
      },
      {
        "name": "我的邮箱",
        "id": 2,
        "spread": true,  //默认展开
        "children": [
          {
            "name": "QQ邮箱",
            "id": 21
          }
        ]
      }
    ]
  });

});